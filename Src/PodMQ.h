#ifndef PODMQ_H
#define PODMQ_H

/* Feel Free To Uncheck */
#define __DEBUG


#include <Arduino.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>


typedef int 		LONG;
typedef bool		BOOL;


typedef struct __PODMQ_HTTP_REQUEST
{
	WiFiClient	wfcCon;
	HTTPClient	hcCon;
	String		sBase;
	String 		sQueue;
	String 		sRequest;
	String 		sPayload;
	String 		sData;
	LONG 		lStatus;
	BOOL		bVerbose;
} PODMQ_HTTP_REQUEST, *LPPODMQ_HTTP_REQUEST;




String
__PodMQExtractData(
	String 		sPayload
){
	String sData = sPayload.substring(sPayload.indexOf("DATA: ")+strlen("DATA: "));
	return sData;
}




bool
PodMQPop(
	LPPODMQ_HTTP_REQUEST	lpphrCtx
){
	if (NULL == lpphrCtx)
	{ return ""; }

	lpphrCtx->sRequest = lpphrCtx->sBase;
	lpphrCtx->sRequest += "?Queue=";
	lpphrCtx->sRequest += lpphrCtx->sQueue;
	
	if (false != lpphrCtx->bVerbose)
	{ lpphrCtx->sRequest += "&Verbose=TRUE"; }

	#ifdef __DEBUG
	Serial.print("PodMQ GET Request: ");
	Serial.println(lpphrCtx->sRequest);
	#endif

	if (true == lpphrCtx->hcCon.begin(lpphrCtx->wfcCon, lpphrCtx->sRequest))
	{
		lpphrCtx->lStatus = lpphrCtx->hcCon.GET();
		
		/* Check For Error */
		if (0 >= lpphrCtx->lStatus)
		{
			#ifdef __DEBUG
			Serial.print("ERROR On HTTP GET: ");
			Serial.println(lpphrCtx->lStatus);
			#endif

			lpphrCtx->hcCon.end();
			return false;
		}

		/* Check for 200 or 204 */
		else if (HTTP_CODE_OK == lpphrCtx->lStatus || HTTP_CODE_NO_CONTENT == lpphrCtx->lStatus)
		{
			lpphrCtx->sPayload = lpphrCtx->hcCon.getString();
			lpphrCtx->sData = __PodMQExtractData(lpphrCtx->sPayload);

			#ifdef __DEBUG
			Serial.print("PodMQ GET Success: ");
			Serial.println(lpphrCtx->sData);
			#endif

			lpphrCtx->hcCon.end();
			return true;
		}

		/* Other Error */
		else 
		{
			#ifdef __DEBUG
			Serial.print("PodMQ GET Error: ");
			Serial.println(lpphrCtx->lStatus);
			Serial.println(lpphrCtx->hcCon.errorToString(lpphrCtx->lStatus));
			#endif 

			lpphrCtx->hcCon.end();
			return false;
		}
	}
}




bool
PodMQCreateQueue(
	LPPODMQ_HTTP_REQUEST	lpphrCtx
){
	if (NULL == lpphrCtx)
	{ return false; }

	lpphrCtx->sRequest = lpphrCtx->sBase;
	lpphrCtx->sRequest += "?Queue=";
	lpphrCtx->sRequest += lpphrCtx->sQueue;
	
	#ifdef __DEBUG
	Serial.print("PodMQ POST Request: ");
	Serial.println(lpphrCtx->sRequest);
	#endif

	if (true == lpphrCtx->hcCon.begin(lpphrCtx->wfcCon, lpphrCtx->sRequest))
	{
		lpphrCtx->lStatus = lpphrCtx->hcCon.POST("");
		
		/* Check For Error */
		if (0 >= lpphrCtx->lStatus)
		{
			#ifdef __DEBUG
			Serial.print("ERROR On HTTP GET: ");
			Serial.println(lpphrCtx->lStatus);
			#endif

			lpphrCtx->hcCon.end();
			return false;
		}

		/* Check for 200 or 204 */
		else if (HTTP_CODE_OK == lpphrCtx->lStatus || HTTP_CODE_NO_CONTENT == lpphrCtx->lStatus)
		{
			lpphrCtx->sPayload = lpphrCtx->hcCon.getString();
			lpphrCtx->sData = __PodMQExtractData(lpphrCtx->sPayload);

			#ifdef __DEBUG
			Serial.print("PodMQ GET Success: ");
			Serial.println(lpphrCtx->sData);
			#endif

			lpphrCtx->hcCon.end();
			return true;
		}

		/* Other Error */
		else 
		{
			#ifdef __DEBUG
			Serial.print("PodMQ GET Error: ");
			Serial.println(lpphrCtx->lStatus);
			Serial.println(lpphrCtx->hcCon.errorToString(lpphrCtx->lStatus));
			#endif 

			lpphrCtx->hcCon.end();
			return false;
		}
	}
}




bool
PodMQPush(
	LPPODMQ_HTTP_REQUEST	lpphrCtx
){
	if (NULL == lpphrCtx)
	{ return false; }

	lpphrCtx->sRequest = lpphrCtx->sBase;
	lpphrCtx->sRequest += "?Queue=";
	lpphrCtx->sRequest += lpphrCtx->sQueue;
	lpphrCtx->sRequest += "&Value=";
	lpphrCtx->sRequest += lpphrCtx->sData;
	
	#ifdef __DEBUG
	Serial.print("PodMQ PUT Request: ");
	Serial.println(lpphrCtx->sRequest);
	#endif

	if (true == lpphrCtx->hcCon.begin(lpphrCtx->wfcCon, lpphrCtx->sRequest))
	{
		lpphrCtx->lStatus = lpphrCtx->hcCon.PUT("");
		
		/* Check For Error */
		if (0 >= lpphrCtx->lStatus)
		{
			#ifdef __DEBUG
			Serial.print("ERROR On HTTP GET: ");
			Serial.println(lpphrCtx->lStatus);
			#endif

			lpphrCtx->hcCon.end();
			return false;
		}

		/* Check for 200 or 204 */
		else if (HTTP_CODE_OK == lpphrCtx->lStatus || HTTP_CODE_NO_CONTENT == lpphrCtx->lStatus)
		{
			lpphrCtx->sPayload = lpphrCtx->hcCon.getString();
			lpphrCtx->sData = __PodMQExtractData(lpphrCtx->sPayload);

			#ifdef __DEBUG
			Serial.print("PodMQ GET Success: ");
			Serial.println(lpphrCtx->sData);
			#endif

			lpphrCtx->hcCon.end();
			return true;
		}

		/* Other Error */
		else 
		{
			#ifdef __DEBUG
			Serial.print("PodMQ GET Error: ");
			Serial.println(lpphrCtx->lStatus);
			Serial.println(lpphrCtx->hcCon.errorToString(lpphrCtx->lStatus));
			#endif 

			lpphrCtx->hcCon.end();
			return false;
		}
	}
}

#endif